# M07 Guestbook App

A simple guest book using Node, Express, BootStrap, EJS

## How to use

navigate to ./m07 folder.

Run npm install to install all the dependencies in the package.json file.

Run node app.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node gbapp.js
```

Point your browser to `http://localhost:8081`. 
